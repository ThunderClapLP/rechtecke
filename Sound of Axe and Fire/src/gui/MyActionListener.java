package gui;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;

import data.Gui_Data;
import logic.Gui_Logic;

public class MyActionListener extends AbstractAction {
	
	private Gui i;
	private Gui_Data gd;
	
	public MyActionListener(Gui i, Gui_Data gd) {
		this.i = i;
		this.gd = gd;
	}
	
		public MyActionListener() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			
			
			if (obj == i.getBtnLogin() )
			{
				gd.login(i.getTextFieldLogin().getText());
			} else if (obj == i.getBtnLogout())
			{
				System.out.println("Logout!");
				gd.logout();
				
				
			} else if (obj == i.getBtnSongAnlegen())
			{
				System.out.println("Song Anlegen!");
				gd.saveSong(gd.getSongListe("").size()+1, i.getTextFieldSongTitel().getText(), i.getTextFieldBand().getText(), i.getTextFieldGenre().getText());
				
			} else if (obj == i.getBtnSuche())
			{
				System.out.println("Suchen! " + i.getTextSuche().getText());
				ArrayList<String> list = gd.getSongListe(i.getTextSuche().getText());
				for (int i = 0; i < list.size(); i++)
				{
					//gd.search();
				}
				//i.getLogic().updateSongListe(results);
				
			} else if (obj == i.getBtnVerwerfen())
			{
				System.out.println("Verwerfen!");
				gd.deleteUser(i.getTextFieldName().getText());
				
			} else if (obj == i.getBtnVote())
			{
				System.out.println("Vote!");
				gd.vote(i.getSongList().getSelectedValue().toString(), i.getLogic().getBen().getName()); //i.getLblAngemeldetAls().getText().substring(16)
				
			} else if (obj == i.getBtnGetUser())
			{
				System.out.println("GetUser!");
				gd.getUser(i.getTextFieldName().getText());
				
			}else if (obj == i.getBtnSpeichern())
			{
				System.out.println("User Speichern!");
				gd.saveUser(i.getTextFieldName().getText(), i.getTextFieldNachname().getText(), i.getTextFieldTitel().getText(), i.getTextFieldHaus().getText());
				
			}else if (obj == i.getBtnErgebnis())
			{
				System.out.println("Ergebnis!");
				gd.getErgebnis();
			}
			
		}	
}
