package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import data.Gui_Data;
import logic.Gui_Logic;

import javax.swing.JTextField;
import java.awt.CardLayout;
import javax.swing.JComboBox;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.border.CompoundBorder;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JTabbedPane;
import javax.swing.JList;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.AbstractListModel;
import javax.swing.JScrollBar;
import java.awt.ScrollPane;
import javax.swing.JScrollPane;

public class Gui extends JFrame {

	private JPanel rootPane;
	private JLabel txtSvenTonkeJulian;
	private JButton btnSuche;
	private JButton btnLogin;
	private JButton btnLogout;
	private JButton btnVerwerfen;
	private JButton btnSpeichern;
	private JButton btnGetUser;
	private JButton btnVote;
	private JButton btnSongAnlegen;
	private Gui_Data gd;
	private Gui_Logic logic;
	private JList songList;
	private JTextField textFieldLogin;
	private JLabel lblAngemeldetAls;
	private JTextField textFieldName;
	private JTextField textFieldNachname;
	private JTextField textFieldTitel;
	private JTextField textFieldHaus;
	private JTextField textFieldSongTitel;
	private JTextField textFieldBand;
	private JTextField textFieldGenre;
	private JTextField textSuche;
	private JButton btnErgebnis;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		logic = new Gui_Logic(this);
		gd = new Gui_Data(logic);
		gd.verbindungAufbauen();
		MyActionListener MyActionListener = new MyActionListener(this, gd);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		rootPane = new JPanel();
		rootPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		rootPane.setLayout(new BorderLayout(0, 0));
		this.setTitle("Song of Axe and Fire");
		setContentPane(rootPane);
		
		txtSvenTonkeJulian = new JLabel();
		txtSvenTonkeJulian.setText("Sven Tonke, Julian Behrends, Sascha Jazairi");
		rootPane.add(txtSvenTonkeJulian, BorderLayout.SOUTH);
		
		JTabbedPane layeredPane = new JTabbedPane(JTabbedPane.TOP);
		rootPane.add(layeredPane, BorderLayout.CENTER);
		
		JPanel loginPane = new JPanel();
		layeredPane.addTab("Login", null, loginPane, null);
		loginPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblNameLogin = new JLabel("Name");
		lblNameLogin.setHorizontalAlignment(SwingConstants.CENTER);
		loginPane.add(lblNameLogin);
		
		textFieldLogin = new JTextField();
		textFieldLogin.setHorizontalAlignment(SwingConstants.CENTER);
		textFieldLogin.setColumns(10);
		loginPane.add(textFieldLogin);
		
		btnLogin = new JButton("Login");
		btnLogin.addActionListener(MyActionListener);
		ImageIcon login = new ImageIcon("src/login.jpg");
		btnLogin.setIcon(transform(login));
		loginPane.add(btnLogin);
		
		btnLogout = new JButton("Logout");
		btnLogout.addActionListener(MyActionListener);
		ImageIcon logout = new ImageIcon("src/logout.png");
		btnLogout.setIcon(transform(logout));
		loginPane.add(btnLogout);
		
		JPanel userPane = new JPanel();
		layeredPane.addTab("User", null, userPane, null);
		//layeredPane.setEnabledAt(1, false);
		userPane.setLayout(new GridLayout(0, 3, 0, 0));
		
		JLabel lblName = new JLabel("Name");
		userPane.add(lblName);
		
		Box horizontalBox = Box.createHorizontalBox();
		userPane.add(horizontalBox);
		
		textFieldName = new JTextField();
		userPane.add(textFieldName);
		textFieldName.setColumns(10);
		
		JLabel lblNachname = new JLabel("Nachname");
		userPane.add(lblNachname);
		
		Box horizontalBox_1 = Box.createHorizontalBox();
		userPane.add(horizontalBox_1);
		
		textFieldNachname = new JTextField();
		userPane.add(textFieldNachname);
		textFieldNachname.setColumns(10);
		
		JLabel lblTitel = new JLabel("Titel");
		userPane.add(lblTitel);
		
		Box horizontalBox_3 = Box.createHorizontalBox();
		userPane.add(horizontalBox_3);
		
		textFieldTitel = new JTextField();
		userPane.add(textFieldTitel);
		textFieldTitel.setColumns(10);
		
		JLabel lblHaus = new JLabel("Haus");
		userPane.add(lblHaus);
		
		Box horizontalBox_4 = Box.createHorizontalBox();
		userPane.add(horizontalBox_4);
		
		textFieldHaus = new JTextField();
		userPane.add(textFieldHaus);
		textFieldHaus.setColumns(10);
		
		btnVerwerfen = new JButton("Verwerfen");
		btnVerwerfen.addActionListener(MyActionListener);
		ImageIcon delet = new ImageIcon("src/delet.png");
		btnVerwerfen.setIcon(transform(delet));
		userPane.add(btnVerwerfen);
		
		btnSpeichern = new JButton("Speichern");
		btnSpeichern.addActionListener(MyActionListener);
		ImageIcon save = new ImageIcon("src/save.png");
		btnSpeichern.setIcon(transform(save));
		userPane.add(btnSpeichern);
		
		btnGetUser = new JButton("Ausf\u00FCllen");
		btnGetUser.addActionListener(MyActionListener);
		ImageIcon load = new ImageIcon("src/load.jpg");
		btnGetUser.setIcon(transform(load));
		userPane.add(btnGetUser);
		
		JPanel songPane = new JPanel();
		layeredPane.addTab("Song", null, songPane, null);
		songPane.setLayout(new GridLayout(0, 3, 0, 0));
		
		JLabel lblSongtitel = new JLabel("Songtitel");
		songPane.add(lblSongtitel);
		
		Box horizontalBox_5 = Box.createHorizontalBox();
		songPane.add(horizontalBox_5);
		
		textFieldSongTitel = new JTextField();
		songPane.add(textFieldSongTitel);
		textFieldSongTitel.setColumns(10);
		
		JLabel lblBandname = new JLabel("Bandname");
		songPane.add(lblBandname);
		
		Box horizontalBox_7 = Box.createHorizontalBox();
		songPane.add(horizontalBox_7);
		
		textFieldBand = new JTextField();
		songPane.add(textFieldBand);
		textFieldBand.setColumns(10);
		
		JLabel lblGenre = new JLabel("Genre ");
		songPane.add(lblGenre);
		
		Box horizontalBox_6 = Box.createHorizontalBox();
		songPane.add(horizontalBox_6);
		
		textFieldGenre = new JTextField();
		songPane.add(textFieldGenre);
		textFieldGenre.setColumns(10);
		
		Box horizontalBox_8 = Box.createHorizontalBox();
		songPane.add(horizontalBox_8);
		
		btnSongAnlegen = new JButton("Song anlegen");
		btnSongAnlegen.setIcon(transform(save));
		btnSongAnlegen.addActionListener(MyActionListener);
		songPane.add(btnSongAnlegen);
		//layeredPane.setEnabledAt(2, false);
		
		JPanel listPane = new JPanel();
		layeredPane.addTab("Songliste", null, listPane, null);
		listPane.setLayout(new BorderLayout(0, 0));
		//layeredPane.setEnabledAt(3, false);
		
		songList = new JList();
		listPane.add(songList, BorderLayout.WEST);
		songList.setValueIsAdjusting(true);
		songList.setModel(new AbstractListModel() {
			String[] values = new String[] {};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(songList);
		listPane.add(scrollPane, BorderLayout.CENTER);
		
		JPanel botPane = new JPanel();
		listPane.add(botPane, BorderLayout.SOUTH);
		
		textSuche = new JTextField();
		botPane.add(textSuche);
		textSuche.setColumns(10);
		
		btnSuche = new JButton("Suche");
		ImageIcon search = new ImageIcon("src/search.png");
		btnSuche.setIcon(transform(search));
		botPane.add(btnSuche);
		
		btnVote = new JButton("Vote");
		botPane.add(btnVote);
		ImageIcon vote = new ImageIcon("src/vote.png");
		btnVote.setIcon(transform(vote));
		
		btnErgebnis = new JButton("Ergebnis");
		ImageIcon ergebnis = new ImageIcon("src/ergebnis.png");
		btnErgebnis.setIcon(transform(ergebnis));
		botPane.add(btnErgebnis);
		
		lblAngemeldetAls = new JLabel("Angemeldet als :");
		rootPane.add(lblAngemeldetAls, BorderLayout.NORTH);
		btnVote.addActionListener(MyActionListener);
		btnSuche.addActionListener(MyActionListener);
		
		logic.updateSongListe(gd.getSongListe(""));

	}
	public JButton getBtnErgebnis() {
		return btnErgebnis;
	}

	public Gui_Logic getLogic() {
		return logic;
	}

	public JList getSongList() {
		return songList;
	}

	public JTextField getTextFieldLogin() {
		return textFieldLogin;
	}

	public JLabel getLblAngemeldetAls() {
		return lblAngemeldetAls;
	}

	public JTextField getTextFieldName() {
		return textFieldName;
	}

	public JTextField getTextFieldNachname() {
		return textFieldNachname;
	}

	public JTextField getTextFieldTitel() {
		return textFieldTitel;
	}

	public JTextField getTextFieldHaus() {
		return textFieldHaus;
	}

	public JTextField getTextFieldSongTitel() {
		return textFieldSongTitel;
	}

	public JTextField getTextFieldBand() {
		return textFieldBand;
	}

	public JTextField getTextFieldGenre() {
		return textFieldGenre;
	}

	public JTextField getTextSuche() {
		return textSuche;
	}

	public JButton getBtnSongAnlegen() {
		return btnSongAnlegen;
	}

	public JButton getBtnSuche() {
		return btnSuche;
	}

	public JButton getBtnVerwerfen() {
		return btnVerwerfen;
	}

	public JButton getBtnSpeichern() {
		return btnSpeichern;
	}

	public JButton getBtnGetUser() {
		return btnGetUser;
	}

	public JButton getBtnVote() {
		return btnVote;
	}

	public JButton getBtnLogin() {
		return btnLogin;
	}

	public JButton getBtnLogout() {
		return btnLogout;
	}
	
	public ImageIcon transform(ImageIcon icon) {
		Image image = icon.getImage();
		Image newimg = image.getScaledInstance(15, 15, java.awt.Image.SCALE_SMOOTH);
		ImageIcon endicon = new ImageIcon(newimg);
		return endicon;
	}
	
}
