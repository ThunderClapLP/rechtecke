package logic;

public class Benutzer {
	private String name = "";
	private String titel = "";
	private String haus = "";
	public Benutzer(String name, String titel, String haus)
	{
		this.name = name;
		this.titel = titel;
		this.haus = haus;
	}
	public String getName() {
		return name;
	}
	public String getTitel() {
		return titel;
	}
	public String getHaus() {
		return haus;
	}
}
