package logic;


import java.util.ArrayList;

import gui.Gui;
import logic.Song;

public class Gui_Logic {
	private Gui gui;
	public Benutzer getBen() {
		return ben;
	}

	public void setBen(Benutzer ben) {
		this.ben = ben;
	}

	private Benutzer ben;
	public Gui_Logic(Gui gui)
	{
		this.gui = gui;
		
	}
	
	//schlauen Kommentierung
	public void updateSongListe(ArrayList<String> list)
	{
			gui.getSongList().setListData(list.toArray());
	}
	
	public void login(String benutzername, String titel, String haus)
	{
		ben = new Benutzer(benutzername, titel, haus);
		gui.getLblAngemeldetAls().setText("Angemeldet als: " + ben.getName());
		gui.getTextFieldLogin().setText("");
		
	}
	
	public void logout() {
		ben = null;
		gui.getLblAngemeldetAls().setText("Angemeldet als:");
	}
	
	public void deleteUserFill() {
		gui.getTextFieldName().setText("");
		gui.getTextFieldNachname().setText("");
		gui.getTextFieldTitel().setText("");
		gui.getTextFieldHaus().setText("");
	}
	
	public void deleteSongFill() {
		gui.getTextFieldBand().setText("");
		gui.getTextFieldGenre().setText("");
		gui.getTextFieldSongTitel().setText("");
	}
}
