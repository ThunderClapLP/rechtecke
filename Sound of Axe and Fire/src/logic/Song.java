package logic;

public class Song {
	private int songID = 0;
	private String songName = "";
	private String bandName = "";
	private String genre = "";
	private int votes = 0;
	public Song(int songID, String songName, String bandName, String genere)
	{
		this.songID = songID;
		this.songName = songName;
		this.genre = genere;
	}
	public int getSongID() {
		return songID;
	}
	public void setSongID(int songID) {
		this.songID = songID;
	}
	public String getSongName() {
		return songName;
	}
	public void setSongName(String songName) {
		this.songName = songName;
	}
	public String getBandName() {
		return bandName;
	}
	public void setBandName(String bandName) {
		this.bandName = bandName;
	}
	public String getGenere() {
		return genre;
	}
	public void setGenere(String genere) {
		this.genre = genere;
	}
	public int getVotes() {
		return votes;
	}
	public void setVotes(int votes) {
		this.votes = votes;
	}

}
