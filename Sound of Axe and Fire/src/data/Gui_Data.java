package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import logic.Gui_Logic;
import logic.Song;

public class Gui_Data {
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/sound_of_axe_and_fire?";
	private String user = "root";
	private String password = "";
	private Connection con;
	private boolean connected = false; 
	private Gui_Logic logic;
	
	public Gui_Data(Gui_Logic logic)
	{
	this.logic = logic;	
	}
	
	
	public boolean verbindungAufbauen()
	{
		try
		{
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausf�hren
			//Statement stmt = con.createStatement();
			connected = true;
				
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		return true;
	}
	
	public ArrayList<String> getSongListe(String suche)
	{
		ArrayList<String> songListe = new ArrayList<String>();
		System.out.println("S: " + suche);
		if (connected == true)
		{
			try {
				Statement stmt = con.createStatement();
				stmt.execute("SELECT * FROM t_songs WHERE titelname LIKE '%" + suche + "%' OR bandname LIKE '%" + suche + "%' OR genre LIKE '%" + suche + "%'");
				ResultSet rs = stmt.getResultSet();
				while (rs.next())
				{
					String songString = "";
					songString += rs.getString("titelname") + " | " + rs.getString("bandname") + " | " + rs.getString("genre");
					songListe.add(songString);
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return songListe;
	}
	

	public void login(String benutzername)
	{
		if (connected == true)
		{
			try {
				Statement stmt = con.createStatement();
				stmt.execute("SELECT * FROM t_gaeste WHERE gaestname='" + benutzername + "'");
				ResultSet rs = stmt.getResultSet();
				while (rs.next())
				{
					logic.login(benutzername, rs.getString("Titel"), rs.getString("Haus"));
				}
			} catch (SQLException e) {
				System.out.println(e);
			}
		} else {
		}
	}
	
	public void logout() {
		logic.logout();
	}
	
	public void saveSong(int id, String songname, String bandname, String genre)  {
		if (connected == true)	
		{
			try {
				Statement stmt = con.createStatement();
				stmt.execute("INSERT INTO t_songs(p_song_id, bandname, titelname, genre) VALUES('"+ id + "','" + bandname + "','" + songname + "','" + genre + "');");
				ResultSet rs = stmt.getResultSet();
				logic.updateSongListe(getSongListe(""));
			} catch(SQLException e) {
				System.out.println(e);
			}
		}
		else System.out.println("Nicht verbunden!");
		
	
	}
	
	public void deleteUser(String name) {
		if (connected == true)
		{
			try {
				Statement stmt = con.createStatement();
				stmt.execute("DELETE FROM t_gaeste WHERE gaestname = '" + name + "';");
			} catch (SQLException e) {
				System.out.println(e);
			}
		}
		else System.out.println("Nicht verbunden!");
		logic.deleteSongFill();
	}


	public void vote(String songname, String benutzername) {
		if (connected ==true)
		{
			try {
				Statement stmt = con.createStatement();
				stmt.execute("INSERT INTO t_votes(f_gastname, f_song_id) VALUES('"+ benutzername + "','" + getSongId(songname) + "');");
			} catch (SQLException e) {
				System.out.println(e);
			}
		}
		else System.out.println("Nicht Verbunden!");
	}
	
	public void getUser(String user) {
		
	}
	
	public void saveUser(String name, String nachname, String titel, String haus) {
		if (connected == true) 
		{
			try {
				Statement stmt = con.createStatement();
				stmt.execute("INSERT INTO t_gaeste(gaestname, titel, nachname, haus) VALUES('"+ name + "','" + titel + "','" + nachname + "','" + haus + "');");
			} catch(SQLException e) {
				System.out.println(e);
			}
		}else System.out.println("Nicht Verbunden!");
		
	}
	
	public void getErgebnis() {
		if (connected == true)
		{
			try {
				Statement stmt = con.createStatement();
				stmt.execute("");
			} catch(SQLException e) {
				System.out.println(e);
			}
		}else System.out.println("Nicht Verbunden!");
	}
	
	protected ResultSet getSongId(String songname) {
		if (connected == true)
		{
			try {
				Statement stmt = con.createStatement();
				stmt.execute("SELECT p_song_id FROM t_songs WHERE titelname = '" + songname + "'");
				ResultSet rs = stmt.getResultSet();
				return rs;
			} catch(SQLException e) {
				System.out.println(e);
			}
		}else System.out.println("Nicht Verbunden!");
		System.out.println("Fehler beim konverten der Song ID");
		return null;
	}


}
