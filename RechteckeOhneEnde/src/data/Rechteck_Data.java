package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import logic.FarbigesRechteck;

public class Rechteck_Data {
	
	private int id = 0;

	public void rechteck_eintragen(FarbigesRechteck fr) {
		// Parameter für Verbindungsaufbau definieren
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost/rechtecke?";
		String user = "root";
		String password = "";

		try {
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausführen
			Statement stmt = con.createStatement();
			stmt.execute("INSERT INTO T_Rechtecke VALUES (" + this.id++ + "," + fr.getPunkt().getX() + " , " + fr.getPunkt().getY() + "," + fr.getBreite() + "," + fr.getHoehe() + "," + fr.getFarbe().getRGB() + ")");
			con.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

}