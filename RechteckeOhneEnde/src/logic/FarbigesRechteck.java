package logic;

import java.awt.Color;

public class FarbigesRechteck extends Rechteck {

	private Color farbe;

	public Color getFarbe() {
		return farbe;
	}

	public void setFarbe(Color farbe) {
		this.farbe = farbe;
	}

	public FarbigesRechteck(Color farbe) {
		super();
		this.farbe = farbe;
	}
	
	public FarbigesRechteck(Punkt punkt, int laenge, int hoehe, Color farbe) {
		super(punkt,laenge, hoehe);
		this.farbe = farbe;
	}
	
}
